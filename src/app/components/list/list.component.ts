import { Component, OnInit, Input } from '@angular/core';
import { PokemonListModel } from 'src/app/models/pokemon-list-item.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-list',
  template: `
    <ul>
      <li *ngFor="let item of listItems?.results">
        {{item.name}}
      </li>
    </ul>
  `,
  styles: []
})
export class ListComponent implements OnInit {
  listItems: PokemonListModel;
  listItems$: Observable<PokemonListModel>

  constructor(public srv: PokemonService, private http: HttpClient){
    //this.listItems = srv.GetPokemon();
    // console.log(this.listItems);

    (async () => {
      this.listItems = 
      await this.http.get<PokemonListModel>("https://pokeapi.co/api/v2/pokemon").toPromise();
    })();
    
  }

  ngOnInit() {
  }

}
