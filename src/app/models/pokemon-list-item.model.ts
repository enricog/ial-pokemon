export interface PokemonListModel {
    count: number;
    next: string;
    previous?: any;
    results: PokemonListItemModel[];
  }
  
export interface PokemonListItemModel {
    name: string;
    url: string;
  }