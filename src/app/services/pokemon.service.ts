import { Injectable } from '@angular/core';
import { PokemonListModel } from '../models/pokemon-list-item.model';


@Injectable({
    providedIn: 'root'
})
export class PokemonService {
    public GetPokemon() : PokemonListModel {
        return {
            count: 2,
            next: null,
            results: [
                {
                    name: 'prova1',
                    url: ''
                },
                {
                    name: 'prova2',
                    url: ''
                }
            ]
        }
    }
}